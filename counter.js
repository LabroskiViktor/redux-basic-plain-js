// REDUCER
function counter(currentState, action) {
  var nextState = {
    count: currentState.count
  };
  switch (action.type) {
    case 'ADD':
      nextState.count = currentState.count + 1;
      return nextState;
      break;
    case 'MINUS':
      nextState.count = currentState.count - 1;
      return nextState;
    case 'RESET':
      nextState.count = 0;
      return nextState;
    default:
      return currentState;
  }
}

var state = { count: 0 };
var store = Redux.createStore(counter, state);
var counterEl = document.getElementById('counter');

function render() {
  var state = store.getState();
  counterEl.innerHTML = state.count.toString();
}

store.subscribe(render);

// ACTIONS
document.querySelector('#add').addEventListener('click', function() {
  store.dispatch({ type: 'ADD' });
});

document.querySelector('#minus').addEventListener('click', function() {
  store.dispatch({ type: 'MINUS' });
});

document.querySelector('#reset').addEventListener('click', function() {
  store.dispatch({ type: 'RESET' });
});
